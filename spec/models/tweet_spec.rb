require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe 'validations' do
    let(:tweet) { Tweet.new(user_id: 1, body: body) }
    let(:subject) { tweet.valid? }
    describe '#characters_limitation' do
      context 'with valid content' do 
        let(:body) { 'a' * 20 }

        it 'creates a valid tweet' do
          subject
          expect(tweet.errors.full_messages_for(:body)).to be_empty
        end
      end
      context 'with invalid content' do 
        let(:body) { 'a' * 200 }

        it 'does not creat a valid tweet' do
          subject
          expect(tweet.errors.full_messages_for(:body)).to match_array(["Body Limit body count exceeded"])
        end
      end
    end
  end
end
