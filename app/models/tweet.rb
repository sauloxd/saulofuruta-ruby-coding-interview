class Tweet < ApplicationRecord
  belongs_to :user

  validate :characters_limitation
  validate :avoid_repeated_tweets

  private

  def characters_limitation
    errors.add(:body, 'Limit body count exceeded') if body.length > 180
  end

  def avoid_repeated_tweets
    errors.add(:body, 'body repeated in the last day') unless Tweet.where(user_id: user_id, body: body).where("created_at > ?", 1.day.ago).empty?
  end
end
